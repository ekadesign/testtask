Реализовать отправку письма которое лежит в resources/views/emails/link.blade.php через очереди

Реализовать синхронизацию новостных лент RSS
Примеры:
newbtc	https://www.newsbtc.com/feed/
coindesk	https://www.coindesk.com/feed/
ccn	https://www.ccn.com/feed/
cointelegraph	https://cointelegraph.com/feed


А так же интерфейс для их администрирования
Создание ленты (поля name и link)
Удаление ленты
Редактирование ленты
Соответственно парсинг всех добавленных лент проходит в автоматическом режиме. Данные о лентах берутся из БД.
На выходе у вас должен быть api со структурой описанной ниже + пагинация

[
    data: {
        {
            "id": 31,
            "title": "Blockchain Exchange Bitsane Introduces Ripple Trading on Consensus 2017",
            "description": "This is a paid-for submitted press release. CCN does not endorse, nor is responsible for any material included below and isn’t responsible for any damages or losses connected with any products or services mentioned in the press release. CCN urges readers to conduct their own research with due diligence into the company, product or service mentioned The post Blockchain Exchange Bitsane Introduces Ripple Trading on Consensus 2017 appeared first on CCN",
            "link": "https://www.ccn.com/blockchain-exchange-bitsane-introduces-ripple-trading-on-consensus-2017/",
            "time": 1524065280,
            "created_at": "2018-04-18 15:37:44",
            "updated_at": "2018-04-18 15:37:44",
            "image": null,
            "tags": [
            "Press Releases"
            ]
        },
    }
]