<?php

namespace App\Http\Controllers;

use App\Mail\SendLink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class IndexController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function success()
    {
        return view('success');
    }
}
